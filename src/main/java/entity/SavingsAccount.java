package entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SavingsAccount extends Account{

    private double interestRate;

    public SavingsAccount(int accountNumber, double interestRate) {
        super(accountNumber);
        this.interestRate = interestRate;
    }

    public double calculateInterest() {
        return balance * interestRate;
    }

    public void applyInterest() {
        double interest = calculateInterest();
        System.out.printf("Interest amount %2.f added to balance%n", interest);
        deposit(interest);
    }

    public void deposit(double amount) {
        if (amount > 0) {
            balance += amount;
            System.out.printf("Amount %.2f you want to deposit.%n", amount);
            // Apply transaction fee.
            balance -= interestRate;
            System.out.printf("Fee %.2f applied.%n", interestRate);
            System.out.printf("Current balance is: %.2f%n", balance);

        } else {
            System.out.println("Negative amount cannot be deposited");
        }
    }

    public void withdraw(double amount) {
        if (amount > 0) {
            if (amount <= balance) {
                System.out.printf("Amount of %.2f withdraw from Account%n", amount);
                balance -= amount;
                System.out.printf("Current balance is: %2.f%n", balance);
            }
        }else {
            System.out.println("Negative amount! Cannot withdraw");
        }

    }

}
