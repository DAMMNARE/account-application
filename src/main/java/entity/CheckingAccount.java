package entity;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CheckingAccount extends Account {
    private static double FEE = 2.5;

    public CheckingAccount(int accountNumber, double fee) {
        super(accountNumber);
        FEE = fee;
    }

    public void deposit(double amount) {
        if (amount > 0) {
            balance += amount;
            System.out.printf("Amount %.2f you want to deposit.%n", amount);
            // Apply transaction fee.
            balance -= FEE;
            System.out.printf("Fee %.2f applied.%n", FEE);
            System.out.printf("Current balance is: %.2f%n", balance);

        } else {
            System.out.println("Negative amount cannot be deposited");
        }
    }

    public void withdraw(double amount) {
        if (amount > 0) {
            if ((amount + FEE) <= 0) {
                System.out.printf("Amount of %.2f withdraw from Account%n", amount);
                balance -= amount;
                balance -= FEE;
                System.out.printf("Fee of %2.f applied%n", FEE);
                System.out.printf("Current balance is: %2.f%n", balance);
            }
        } else {
            System.out.println("Negative amount! Cannot withdraw");
        }

    }
}
