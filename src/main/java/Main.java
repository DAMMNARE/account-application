import entity.Account;
import entity.CheckingAccount;
import entity.SavingsAccount;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Account accounts[] = new Account[10];
        int numAccount = 0;

        int choice;

        do {
            choice = menu(scanner);
            System.out.println();
            if (choice == 1) {
                accounts[numAccount++] = createAccount(scanner);
            }else if (choice == 2) {
                doDeposit(accounts, numAccount, scanner);
            } else if (choice == 3) {
                doWithdraw(accounts, numAccount, scanner);
            } else if (choice == 4) {
                applyInterest(accounts, numAccount, scanner);
            }else {
                System.out.println("GoodBye!");
            }
            System.out.println();
        } while (choice != 5);
    }

    public static int accountMenu(Scanner scanner) {
        System.out.println("Select account type:");
        System.out.println("1. Checking account;");
        System.out.println("2. Savings account;");

        int choice;
        do {
            System.out.print("Enter choice: ");
            choice = scanner.nextInt();
        } while (choice < 1 || choice > 2);
        return choice;
    }

    public static int searchAccount(Account accounts[], int count, int accountNumber) {
        for (int i = 0; i < count; i++) {
            if (accounts[i].getAccountNumber() == accountNumber) {
                return i;
            }
        }
        return -1;
    }

    public static void doDeposit(Account accounts[], int count, Scanner scanner) {
        System.out.printf("\nPlease enter account number: ");
        int accountNumber = scanner.nextInt();

        //Search for account.
        int index = searchAccount(accounts, count, accountNumber);

        if (index >= 0) {
            //Deposit amount.
            System.out.print("Please enter deposit amount: ");
            double amount = scanner.nextInt();

            accounts[index].deposit(amount);
        } else {
            System.out.println("No account exist with the account number: " + accountNumber);
        }
    }

    public static void doWithdraw(Account accounts[], int count, Scanner scanner) {
        System.out.printf("\nPlease enter account number: ");
        int accountNumber = scanner.nextInt();
        int index = searchAccount(accounts, count, accountNumber);

        if (index >= 0) {
            System.out.printf("Please enter withdraw amount: ");
            double amount = scanner.nextInt();
            accounts[index].withdraw(amount);
        } else {
            System.out.println("No account exist with the account number: " + accountNumber);
        }
    }

    public static void applyInterest(Account accounts[], int count, Scanner scanner) {
        System.out.printf("\nPlease enter account number: ");
        int accountNumber = scanner.nextInt();
        int index = searchAccount(accounts, count, accountNumber);

        if (index >= 0) {
            ((SavingsAccount) accounts[index]).applyInterest();
        } else {
            System.out.println("No account exist with the account number: " + accountNumber);
        }
    }

    public static Account createAccount(Scanner scanner) {
        Account account = null;
        int choice = accountMenu(scanner);

        int accountNumber;
        System.out.println("Enter your account number: ");
        accountNumber = scanner.nextInt();

        //Checking account
        if (choice == 1) {
            System.out.println("Enter transaction fee: ");
            double fee = scanner.nextDouble();
            account = new CheckingAccount(accountNumber, fee);

            //Savings account
        } else {
            System.out.println("Enter your interest rate: ");
            double interestRate = scanner.nextDouble();
            account = new SavingsAccount(accountNumber, interestRate);
        }
        return account;
    }

    public static int menu(Scanner scanner) {
        System.out.println("Bank Account Menu:");
        System.out.println("1. Create new account;");
        System.out.println("2. Deposit funds;");
        System.out.println("3. Withdraw funds;");
        System.out.println("4. Apply interest;");
        System.out.println("5. Quit");

        int choice;
        do {
            System.out.print("Enter choice: ");
            choice = scanner.nextInt();
        } while (choice < 1 || choice > 5);
        return choice;
    }
}